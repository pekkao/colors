import { Component, OnInit } from '@angular/core';
import { Color } from '../../color';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  colors: Color[] = [];
  selectedColor: Color;
  colorCounter: number;

  constructor() {}

  ngOnInit() {
    fetch('../../assets/data/colors.json').then(res => res.json())
    .then(json => {
      this.colors = json;
      this.selectedColor = this.colors[this.colorCounter];
    });
  }

  setColor(index: number) {
    this.selectedColor = this.colors[index];
  }

}
