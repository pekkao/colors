export class Color {
    name: string;
    code: string;
    shades: string[];
}
